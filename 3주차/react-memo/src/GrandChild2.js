import React from "react";

const GrandChild2 = () => {
    console.log('손녀컴포넌트 렌더링')
    return (
        <div className="child2">
            <h1>손녀 컴포넌트</h1>
        </div>
    )
}

export default React.memo(GrandChild2);