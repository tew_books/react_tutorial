import React from "react";

const GrandChild1 = () => {
    console.log('손자컴포넌트 렌더링')
    return (
        <div className="child1">
            <h1>손자 컴포넌트</h1>
        </div>
    )
}

export default React.memo(GrandChild1);