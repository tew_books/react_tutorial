import { useCallback, useMemo, useState } from 'react';
import './App.css';
import Child from './Child';

function App() {
  const [num, setNum] = useState(0);
  console.log('부모컴포넌트 렌더링')

  return (
    <div className="App">
      <h1>부모 컴포넌트</h1>
      <button onClick={()=>{setNum(num+1)}}>부모버튼</button>
      <Child num={num} />
    </div>
  );
}

export default App;
