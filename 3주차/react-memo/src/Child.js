import GrandChild1 from "./GrandChild1";
import GrandChild2 from "./GrandChild2";
import React from "react";

const Child = ({num}) => {
    console.log('자식컴포넌트 렌더링')
    return (
        <div className="child">
            <h1>자식 컴포넌트</h1>
            {num}
            <GrandChild1 />
            <GrandChild2 />
        </div>
    )
}

export default React.memo(Child);