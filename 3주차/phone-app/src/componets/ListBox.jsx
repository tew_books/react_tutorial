import User from './UserList';
import Filter from './UserFilter.jsx';
import React, { useState, useMemo, useEffect } from 'react';
import { useCallback } from 'react';

const ListBox = ({users, onRemoveUser}) => {
    const [selectVal, setSelectVal] = useState('all');

    const selectChangeHandler = (자식에서선택한val) => {
        setSelectVal(자식에서선택한val)
        // console.log('자식에서선택한val', 자식에서선택한val)
    }

    const filterUsers = users.filter(user => {
        if(selectVal === 'all') {
            return user
        }
        return user.info === selectVal;
    })

    // const filterMobileUsers = (users) => {
    //     console.log("엄청나게 복잡한 함수입니다!!")
    //     return users.filter((user) => user.info === '📞mobile').length;
    // }

    // const count = useMemo(()=> 
    //     filterMobileUsers(users)
    // , [users])

    const getAllUsersCount = useCallback(() => {
        console.log('getAllUserCount:', users.length);
        return;
    }, [users])
    
    useEffect(()=> {
        console.log('getAllUserCount 함수가 변경될 때 호출')
    }, [getAllUsersCount])

    // const name1 = '홍길동'
    // const name2 = '홍길동'

    // const obj1 = {
    //     name: '홍길동',
    //     age: 27
    // }
    // // 역삼동 1번지

    // const obj2 = {
    //     name: '홍길동',
    //     age: 27
    // }
    //  // 역삼동 33번지

    // console.log(obj1 === obj2)

    return (
        <>
        {/* <h1>{count}</h1> */}
        <button onClick={getAllUsersCount}>함수 호출</button>
        <Filter onSelect={selectChangeHandler} />
        <div className='user-list-wrap'>
            {
            filterUsers.map((user) => (
                <User user={user} key={user.id} onRemoveUser={onRemoveUser} />
            ))
            }
        </div>
        </>
    )
}

export default ListBox