

const UserList = (props) => {
    // console.log(props)

    const clickHandler = () => {
        props.onRemoveUser(props.user.id)
        // console.log('클릭된 아이디', props.user.id)
    }

    return (
        <div className='list-box' onClick={clickHandler}>
            <h2>{props.user.name}</h2>
            <b>{props.user.phone}</b><span>{props.user.info}</span>
        </div>
    )
}

export default UserList;