import './App.css';
import React, { useRef, useState } from 'react';
import User from './componets/UserList';
import AddUser from './componets/AddUser';
import ListBox from './componets/ListBox';

function App() {

  const [users, setUsers] = useState(
    [
      {
        id: 1,
        name: '짱구',
        phone: '010-1234-1023',
        info: '📞mobile',
      },
      {
        id: 2,
        name: '짱아',
        phone: '02-2345-3442',
        info: '🏠home',
      },
      {
        id: 3,
        name: '흰둥이',
        phone: '010-1321-3423',
        info: '📞mobile',
      },
    ]
  )

  const addUserHandler = (자식에서받아온data) => {
    // console.log('자식에서받아온데이터', 자식에서받아온data)
    setUsers((prevUser)=> {
      return [자식에서받아온data, ...prevUser]
    })
  }

  const removerUserHandler = (자식에서클릭한idx) => {
    setUsers((prevUser) => {
      const updateUsers = prevUser.filter((user) => 
        user.id !== 자식에서클릭한idx)
        return updateUsers;
    })
    // console.log('app.js', 자식에서클릭한idx)
  }

  return (
    <div className='container'>
      <AddUser onAddUser={addUserHandler} />
      <ListBox users={users} onRemoveUser={removerUserHandler} />
    </div>
  ) 
}

export default App;
