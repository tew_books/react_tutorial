


const UserFilter = (props) => {

    const changeHandler = (e) => {
        props.onSelect(e.target.value)
        // console.log(e.target.value)
    }

    return (
        <select onChange={changeHandler}>
            <option value="all">전체</option>
            <option value="📞mobile">모바일</option>
            <option value="🏠home">홈</option>
        </select>
    )
}

export default UserFilter;