import User from './UserList';
import Filter from './UserFilter.jsx';
import React, { useState, useMemo } from 'react';
import { useCallback } from 'react';

const ListBox = ({users, onRemoveUser}) => {
    console.log(users)
    const [selectVal, setSelectVal] = useState('all');

    const selectChangeHandler = (자식에서선택한val) => {
        setSelectVal(자식에서선택한val)
        // console.log('자식에서선택한val', 자식에서선택한val)
    }

    const filterUsers = users.filter(user => {
        if(selectVal === 'all') {
            return user
        }
        return user.info === selectVal;
    })

    const filterUsersCount = (users) => {
        return users.length;
    }

    const count = useMemo(()=> 
        filterUsersCount(users)
    , [users])

    // const getAllUsersCount = useCallback(() => {
    //     console.log('getAllUserCount:', users.length);
    //     return;
    // }, [users])
    
    // useEffect(()=> {
    //     console.log('getAllUserCount 함수가 변경될 때 호출')
    // }, [getAllUsersCount])

    // const name1 = '홍길동'
    // const name2 = '홍길동'

    // const obj1 = {
    //     name: '홍길동',
    //     age: 27
    // }
    // // 역삼동 1번지

    // const obj2 = {
    //     name: '홍길동',
    //     age: 27
    // }
    //  // 역삼동 33번지

    // console.log(obj1 === obj2)

    return (
        <>
        <p><span>친구👭 </span><b>{count}</b></p>
        {/* <button onClick={getAllUsersCount}>함수 호출</button> */}
        <Filter onSelect={selectChangeHandler} />
        <div className='user-list-wrap'>
            {
            filterUsers.map((user) => (
                <User user={user} key={user.id} onRemoveUser={onRemoveUser} />
            ))
            }
        </div>
        </>
    )
}

export default ListBox;