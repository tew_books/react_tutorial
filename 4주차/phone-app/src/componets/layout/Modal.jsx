

const Modal = ({title, content, onClose, onCancel}) => {
    return (
        <>
          <div className="modal-back"></div>  
          <div className="modal-wrap">
            <header>
                <h2>{title}</h2>
                <button id="x-btn" onClick={onClose}>✖️</button>
            </header>
            <div className="content">
                {content}
            </div>
            <footer>
                <button onClick={onCancel}>확인</button>
            </footer>
          </div>
        </>
    )
}

export default Modal;