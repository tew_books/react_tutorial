import React, {useState, useRef} from "react";
import Modal from './layout/Modal';

const AddUser = (props) => {
  const [error, setError] = useState('');

  const [inputs, setInputs] = useState({
    userName: '',
    phone: ''
  })

  const changeHandler = e => {
    let { name, value } = e.target;

    if(name === 'phone') {
      value = value.replace(/[^0-9]/g, '').replace(/^(\d{2,3})(\d{3,4})(\d{4})$/, `$1-$2-$3`);
    }
   
    setInputs({
      ...inputs,
      [name]: value
    })
  }


  const addUserHandler = () => {

    const regex= /[a-z0-9]|[ \[\]{}()<>?|`~!@#$%^&*-_+=,.;:\"'\\]/g;
    const useName = inputs.userName.replace(regex, '');

    let userInfo = '';

    if((/\d{3}-\d{3,4}-\d{4}/g).test(inputs.phone)) {
      userInfo = '📞mobile'
    } else {
      userInfo = '🏠home'
    }

    const userId = Math.random();

    const user = {
      id: userId,
      name: useName,
      phone: inputs.phone,
      info: userInfo
    }

    if(inputs.userName === '' || inputs.phone === '' ) {
      return setError({
        title: 'Error!!',
        content: '이름과 전화번호 정보를 입력해주세요!'
      })
    }

    props.onAddUser(user);

    setInputs({
      userName: '',
      phone : ''
    })


    props.onToggle();
  }

  const closeHandler = () => {
    setError('')
  }

  
    return (
      <>
       {
        error &&
        <Modal title={error.title} content={error.content} onClose={closeHandler} onCancel={closeHandler} />
       }
        <div className='input-wrap'>
        <input
          name="userName"
          placeholder="이름"
          onChange={changeHandler}
          value={inputs.userName}
        />
        <input
          name="phone"
          placeholder="전화번호"
          onChange={changeHandler}
          value={inputs.phone}
        />
        <button onClick={addUserHandler}>추가</button>
      </div>
      </>
    )
}

export default AddUser;
