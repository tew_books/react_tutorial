import { useState, useContext } from 'react';
import Modal from './layout/Modal';
import { UserContext } from '../store/UserContext';

const UserList = (props) => {

    const name = useContext(UserContext);

    const [modalTxt, setModalTxt] = useState('');

    const clickHandler = () => {
        // return alert('삭제하시겠습니까?')

        return setModalTxt({
            title: '경고',
            content: '정말 삭제하시겠습니까?'
        })
    }

    const closeHandler = () => {
        setModalTxt('');
    }

    const cancelHandler = () => {
        props.onRemoveUser(props.user.id)
    }

    return (
        <>
        {
            modalTxt && 
            <Modal 
                title={modalTxt.title} 
                content={modalTxt.content} 
                onClose={closeHandler} 
                onCancel={cancelHandler}
            />
        }
        <div className='list-box' onClick={clickHandler}>
            <p>{name}님의 전화번호부</p>
            <h2>{props.user.name}</h2>
            <b>{props.user.phone}</b><span>{props.user.info}</span>
        </div>
        </>

    )
}

export default UserList;