import React, { useState } from 'react';
import AddUser from './componets/AddUser';
import ListBox from './componets/ListBox';
import './App.css';
import Header from './componets/layout/Header';
import { UserContext } from './store/UserContext';

function App() {

  const [visible, setVisible] = useState(false);

  const [users, setUsers] = useState(
    [
      {
        id: 1,
        name: '짱구',
        phone: '010-1234-1023',
        info: '📞mobile',
      },
      {
        id: 2,
        name: '짱아',
        phone: '02-2345-3442',
        info: '🏠home',
      },
      {
        id: 3,
        name: '흰둥이',
        phone: '010-1321-3423',
        info: '📞mobile',
      },
    ]
  )

  const addUserHandler = (자식에서받아온data) => {
    setUsers((prevUser)=> {
      return [자식에서받아온data, ...prevUser]
    })
  }

  const removerUserHandler = (자식에서클릭한idx) => {
    setUsers((prevUser) => {
      const updateUsers = prevUser.filter((user) => 
        user.id !== 자식에서클릭한idx)
        return updateUsers;
    })
  }

  const toggleHandle = () => {
    setVisible(false)
  }


  return (
    <UserContext.Provider value="짱구">
      <Header />
      <div className='container'>
        {
          visible ? <AddUser onAddUser={addUserHandler} onToggle={toggleHandle} /> :
          <button onClick={()=>{setVisible(true)}} id='visible-form-btn'>새로운 연락처 등록</button>
        }
        <ListBox users={users} onRemoveUser={removerUserHandler} />
      </div>
    </UserContext.Provider>
  ) 
}

export default App;
