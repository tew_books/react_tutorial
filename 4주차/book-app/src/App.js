import { useCallback, useEffect, useState } from 'react';
import ListBox from './components/ListBox';

function App() {
  const [data, setData] = useState('');
  const [loading, setLoding] = useState(false);

  const fetchBooksHandler = useCallback(async() => {
    setLoding(true)

    try {
      const response = await fetch(`https://www.googleapis.com/books/v1/volumes?q=search+terms`)
    
      const data = await response.json()
      console.log(data)

      const newData = data.items.map(item => {
        return {
          id: item.id,
          title: item.volumeInfo.title,
          img: item.volumeInfo.imageLinks.thumbnail,
          date: item.volumeInfo.publishedDate,   //출판일
          content: item.volumeInfo.description, // 소개
          pages: item.volumeInfo.pageCount      //총페이지
        }
      })

      console.log('뉴데이터,', newData)
      setData(newData);
    } catch(error) {
      console.log(error);
    }
    setLoding(false)

  }, []);

  useEffect(()=> {
    fetchBooksHandler();
  }, [fetchBooksHandler])

  return (
    <div className="App">
      {
        !loading && data ?
        <ListBox data={data} /> :
        <p>로딩중 ...</p>
      }
    </div>
  );
}

export default App;
