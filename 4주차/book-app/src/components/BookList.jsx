import styles from './BookList.module.css';

const BookList = ({data, onClickList}) => {
    // console.log('데이터', data)

    const toggleHandler = () => {
        onClickList(data.id)
    }

    return (
        <article className={styles.container} onClick={toggleHandler} >
            <img src={data.img} alt={data.title} />
            <div className={styles.contents}>
                <h1>{data.title}</h1>
                <p>출간일: {data.date}</p>
            </div>
        </article>
    )
}

export default BookList;