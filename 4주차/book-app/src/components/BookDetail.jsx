import styles from './BookDetail.module.css';

const BookDetail = ({data, onClickBack}) => {
    console.log('dd', data)

    const backHandler = () => {
        onClickBack('')
    }

    return (
      <>
        <button onClick={backHandler}>⬅️</button>
        <h1 className={styles.title}>상세페이지입니다.</h1>
        <article className={styles.container}>
            <img src={data.img} alt={data.title} />
            <h1>{data.title}</h1>
            <p>{data.content}</p>
            <p>출판일: {data.date}</p>
            <p>총페이지: {data.pages}</p>
        </article>
      </>
    )
}

export default BookDetail;