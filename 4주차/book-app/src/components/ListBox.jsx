import BookList from './BookList';
import BookDetail from './BookDetail';
import { useState } from 'react';

const ListBox = (props) => {
    const [clickId, setClickId] = useState('');

    const filterBook = props.data.filter(item => {
        return item.id === clickId;
    })

    console.log('props', props)

    const toggleHandler = (id) => {
        setClickId(id)
    }

    return (
        <>
            {

                clickId ?
                filterBook.map((item) => (
                    <BookDetail data={item} key={item.id} onClickBack={toggleHandler} />
                )) :
                props.data.map((item) => (
                    <BookList data={item} key={item.id} onClickList={toggleHandler} />
                ))
            }
        </>
    )
}

export default ListBox;